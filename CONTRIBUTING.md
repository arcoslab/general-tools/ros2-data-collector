Please do not push directly to master.

We use semantic versioning, which is what ROS2 recomends. So for each release we will have a development branch named devX.X.X. So for example for version 0.2.4 the development branch will be dev0.2.4.

If you want to contribute to this project, create your own branch (branches are free and easy to create, don't be shy, create as many branches as you want). And when the feature that you want to add is ready, create a Merge Request to the current development branch.

Once features for a particular branch are ready, the branch will merge to master.
