# ROS2 Data Collector

This is a simple package with nodes that take information available in ROS2
topics and sends it to a time series database (in our case it is InfluxDB)

The information that we currently support is:

- [ ] Base power management
- [ ] Kuka arms state
- [ ] Wessling hands state
